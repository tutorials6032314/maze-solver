# Maze Solver

This is my version of following the excellent Real Python tutorial develop a 
[maze solver]([https://realpython.com/python-maze-solver/]) using graphs. 

## Installation
This is a regular python package. Installation can be done by cloning this 
repository and installing it using pip.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Authors and acknowledgment
This code is based on the tutorial at Real Python by Bartosz Zaczyński. 

## License
This code is licensed under MIT license. 

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
